<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Arriendo de Pelicula</title>
        <link href="css/Estilos.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    </head>
    <body style="background-color: rgb(186,240,255);">
        <h1 style="text-align: center; font-size: 45px;">Arriendo de Peliculas</h1>
        <p style="text-align: center; font-size: 15px; margin-bottom: 20px;">Elija la pelicula que se va a prestar</p>
        <div class="container" style="background-color: rgb(171,194,207); padding-bottom: 250px; border-radius: 10px 10px 10px 10px;">
                <div class="row">
                    <form action ="registarcontroller" metod ="get">
                    <div class="col-md-4 col-md-offset-4 text-center">
                        <p style="margin-top: 60px; font-size: 20px;" >Rut Cliente</p>
                        <input type="text" id="name" class="form-control"name = "Rut" maxlength="12" required>
                        <p id="formu">Nombre cliente</p>
                        <input type="text" id="name" class="form-control"name = "Nombre" required>
                        <p id="formu">Apellido cliente</p>
                        <input type="text" id="name" class="form-control"name = "Apellido" required>
                    
                        <p id="formu">Pelicula</p>
                        <select class="custom-select" id="combopeli"name = "pelicula">
                            <c:forEach var="pelicula" items="${peliculas}"> 
                                <option value="${pelicula.getIdPelicula()}">${pelicula.getNombrePelicula()}</option>
                            </c:forEach>
                        </select>    
                   
                        <p id="formu">Dias</p>
                        <select class="custom-select" id="combopeli"name = "Dias">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>    

                            <input type="submit" id="submit" class="form-control" value="Iniciar Sesion" style="margin-top: 10px;">
 
                        </div>
                        </form>
                
            
            </div>
        
        
        
    </body>
</html>
