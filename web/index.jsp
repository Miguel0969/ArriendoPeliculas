
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Arriendo Peliculas</title>
        <link href="css/Estilos.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    </head>
    <body style="background-color: rgb(186,240,255);">
        <div class="container" style="background-color: rgb(171,194,207); padding-bottom: 250px;">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 text-center" style="margin-top: 150px;">
                            <div class="search-box">
                                    <div class="caption">
                                        <h3 id="ArriendoPelicula">Arriendos de peliculas</h3>
                                            <p>Ingresar Usuario</p>
                                    </div>
                                    <form action="login" class="loginForm" method="post">
                                            <div class="input-group">
                                                <input type="text" id="name" class="form-control" placeholder="Usuario" name="usu">
                                                <input type="password" id="paw" class="form-control" placeholder="Contraseña" name="pass">                              
                                                <input type="submit" id="submit" class="form-control" value="Iniciar Sesion">   
                                            </div>
                                    </form>
                            </div>
                    </div>
            </div>
        </div> 
    </body>
</html>
