
package Entidad;


public class Usuario {
    
   private int IdUsuario;
   private String Nickname;
   private String Contraseña;

    public Usuario(int IdUsuario) {
        this.IdUsuario = IdUsuario;
    }
    
    public Usuario(String Nickname, String Contraseña) {
        this.Nickname = Nickname;
        this.Contraseña = Contraseña;
    }

    public int getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(int IdUsuario) {
        this.IdUsuario = IdUsuario;
    }

    public String getNickname() {
        return Nickname;
    }

    public void setNickname(String Nickname) {
        this.Nickname = Nickname;
    }

    public String getContraseña() {
        return Contraseña;
    }

    public void setContraseña(String Contraseña) {
        this.Contraseña = Contraseña;
    }

    @Override
    public String toString() {
        return "Usuario{" + "IdUsuario=" + IdUsuario + ", Nickname=" + Nickname + ", Contrase\u00f1a=" + Contraseña + '}';
    }

    
  

   

    
}


