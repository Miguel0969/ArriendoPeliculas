
package Entidad;


public class Arriendo {
    
    private int IdArriendo;
    private int Dias;
    private int IdUsuario;
    private String RutUsuario;
    private int IdPelicula;
    private String NombreCliente;
    private String ApellidoCliente;

    public int getIdArriendo() {
        return IdArriendo;
    }

    public void setIdArriendo(int IdArriendo) {
        this.IdArriendo = IdArriendo;
    }

    public int getDias() {
        return Dias;
    }

    public void setDias(int Dias) {
        this.Dias = Dias;
    }

    public int getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(int IdUsuario) {
        this.IdUsuario = IdUsuario;
    }

    public String getRutUsuario() {
        return RutUsuario;
    }

    public void setRutUsuario(String RutUsuario) {
        this.RutUsuario = RutUsuario;
    }

    public int getIdPelicula() {
        return IdPelicula;
    }

    public void setIdPelicula(int IdPelicula) {
        this.IdPelicula = IdPelicula;
    }

    public String getNombreCliente() {
        return NombreCliente;
    }

    public void setNombreCliente(String NombreCliente) {
        this.NombreCliente = NombreCliente;
    }

    public String getApellidoCliente() {
        return ApellidoCliente;
    }

    public void setApellidoCliente(String ApellidoCliente) {
        this.ApellidoCliente = ApellidoCliente;
    }

    @Override
    public String toString() {
        return "Arriendo{" + "IdArriendo=" + IdArriendo + ", Dias=" + Dias + ", IdUsuario=" + IdUsuario + ", RutUsuario=" + RutUsuario + ", IdPelicula=" + IdPelicula + ", NombreCliente=" + NombreCliente + ", ApellidoCliente=" + ApellidoCliente + '}';
    }

    
    
}
