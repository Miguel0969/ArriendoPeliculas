
package Entidad;


public class Categoria {
    
    private int IdCategoria;
    private String NombreCategoria;

    public int getIdCategoria() {
        return IdCategoria;
    }

    public void setIdCategoria(int IdCategoria) {
        this.IdCategoria = IdCategoria;
    }

    public String getNombreCategoria() {
        return NombreCategoria;
    }

    public void setNombreCategoria(String NombreCategoria) {
        this.NombreCategoria = NombreCategoria;
    }

    @Override
    public String toString() {
        return "Categoria{" + "IdCategoria=" + IdCategoria + ", NombreCategoria=" + NombreCategoria + '}';
    }


    

   

  
    
    
}
