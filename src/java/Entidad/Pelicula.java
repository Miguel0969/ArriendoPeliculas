
package Entidad;

public class Pelicula {
    
    private int IdPelicula;
    private String NombrePelicula;
    private int IdCategoria;

    public int getIdPelicula() {
        return IdPelicula;
    }

    public void setIdPelicula(int IdPelicula) {
        this.IdPelicula = IdPelicula;
    }

    public String getNombrePelicula() {
        return NombrePelicula;
    }

    public void setNombrePelicula(String NombrePelicula) {
        this.NombrePelicula = NombrePelicula;
    }

    public int getIdCategoria() {
        return IdCategoria;
    }

    public void setIdCategoria(int IdCategoria) {
        this.IdCategoria = IdCategoria;
    }

    @Override
    public String toString() {
        return "Pelicula{" + "IdPelicula=" + IdPelicula + ", NombrePelicula=" + NombrePelicula + ", IdCategoria=" + IdCategoria + '}';
    }

    

 
  

   
    
    
    
}
