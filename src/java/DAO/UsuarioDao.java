
package DAO;

import Entidad.Usuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioDao {
    private dbConect conn;

    public UsuarioDao(dbConect conn) {
        this.conn = conn;
    }
    
    public Usuario login(String user, String pass) {

        try {
            String sql = "select * from usuario where Nickname=? and Contraseña = ?";
            PreparedStatement preparedStatement = conn.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, user);
            preparedStatement.setString(2, pass);
            ResultSet rs = preparedStatement.executeQuery();
            Usuario usuario = new Usuario(0);
            while (rs.next()) {
                // Create an object for the user
                usuario.setIdUsuario(rs.getInt("Id_Usuario"));
                usuario.setNickname(rs.getString("Nickname"));
                usuario.setContraseña(rs.getString("Contraseña"));
            }
            return usuario;
        } catch (SQLException e) {
            System.out.println("Error UsuarioDao.login: " + e.getMessage());
            return null;
        }
    }

    
}
