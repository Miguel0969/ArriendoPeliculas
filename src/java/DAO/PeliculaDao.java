
package DAO;

import Entidad.Pelicula;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class PeliculaDao {
    
    private dbConect conn;

    public PeliculaDao(dbConect conn) {
        this.conn = conn;
    }
    
     public List<Pelicula> getComboPelicula() {
         PreparedStatement getcombo;
        try {

            getcombo = conn.getConnection().prepareStatement(
                    "select Id_Pelicula, Nombre_Pelicula from pelicula order by Id_Pelicula");
            
            ResultSet rs = getcombo.executeQuery();
            List<Pelicula> list = new LinkedList<>();
            Pelicula pelicula;
            while (rs.next()) {
                pelicula = new Pelicula();
                pelicula.setIdPelicula(rs.getInt("Id_Pelicula"));
                pelicula.setNombrePelicula(rs.getString("Nombre_Pelicula"));      
                list.add(pelicula);
                
                System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                System.out.println(list);
            }
            return list;

        } catch (SQLException e) {            
            System.out.println("Error: " + e.getMessage());
            return null;
        }
    }
    
}
