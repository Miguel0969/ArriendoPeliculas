/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entidad.Arriendo;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author TOSHIBA L835
 */
public class ArriendoDao {
    
    private dbConect conn;

    public ArriendoDao(dbConect conn) {
        this.conn = conn;
    }
    
    public boolean save(Arriendo arriendo){
        PreparedStatement saveArriendo;
        
        try{
            saveArriendo = conn.getConnection().prepareStatement(
                    "INSERT INTO arriendo (Id_Arriendo,Id_Pelicula, Dias, Rut_Cliente, Nombre_Cliente, Apellido_Cliente)"
                            + "VALUES (?,?,?,?,?,?)");
            saveArriendo.setInt(1, arriendo.getIdArriendo());
            saveArriendo.setInt(2, arriendo.getIdPelicula());
            saveArriendo.setInt(3, arriendo.getDias());
            saveArriendo.setString(4, arriendo.getRutUsuario());
            saveArriendo.setString(5, arriendo.getNombreCliente());
            saveArriendo.setString(6, arriendo.getApellidoCliente());
            
            saveArriendo.executeUpdate();
            
            return true;
        } catch (SQLException e){
            System.out.println("Error: "+e);
            return false;
       
        }
    }
        
    
}
