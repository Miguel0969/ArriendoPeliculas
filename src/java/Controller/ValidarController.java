
package Controller;

import DAO.UsuarioDao;
import DAO.dbConect;
import Entidad.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "Valida", urlPatterns = {"/login"})
public class ValidarController extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
            HttpSession session = request.getSession();
            Usuario usu= (Usuario)session.getAttribute("usuario");
            RequestDispatcher rd;
            if (usu == null) {
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }else{
                request.getRequestDispatcher("combocontroller").forward(request, response);
            }
            
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        String nick,pass;
        nick= request.getParameter("usu");
        pass= request.getParameter("pass");
        dbConect conn = new dbConect();
        UsuarioDao usDa = new UsuarioDao(conn);
        Usuario usuario = usDa.login(nick, pass);
        conn.disconnect();
        HttpSession session = request.getSession();
        session.setAttribute("usuario", null);
        if(usuario.getIdUsuario()>0){
                Usuario usu= new Usuario(nick,pass);
                session.setAttribute("usuario", usu);
                request.getRequestDispatcher("combocontroller").forward(request, response);
        }else{         
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }   
        
    }
    
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
