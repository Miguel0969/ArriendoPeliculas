/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.PeliculaDao;
import DAO.dbConect;
import Entidad.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author miguel
 */
public class ComboController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
            HttpSession session = request.getSession();
            Usuario usu= (Usuario)session.getAttribute("usuario");
            RequestDispatcher rd;
            if (usu == null) {
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }

    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        HttpSession session = request.getSession();
        Usuario usu= (Usuario)session.getAttribute("usuario");
        dbConect conect = new dbConect();
        
        RequestDispatcher rd;
        if (usu == null) {
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }else{
            
            PeliculaDao peli = new PeliculaDao(conect);
            request.setAttribute("peliculas", peli.getComboPelicula());
            System.out.println(request.getAttribute("peliculas"));
            request.getRequestDispatcher("/Arriendo.jsp").forward(request, response);
        }
        
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
