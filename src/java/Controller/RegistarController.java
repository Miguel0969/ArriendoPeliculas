/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.ArriendoDao;
import DAO.PeliculaDao;
import DAO.dbConect;
import Entidad.Arriendo;
import Entidad.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author TOSHIBA L835
 */
@WebServlet(name = "RegistarController", urlPatterns = {"/registarcontroller"})
public class RegistarController extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        String Rut = request.getParameter("Rut");
        String Nombre = request.getParameter("Nombre");
        String Apellido = request.getParameter("Apellido");
        String Pelicula = request.getParameter("pelicula");
        String Dias = request.getParameter("Dias");
       
        
        dbConect conect = new dbConect();
        ArriendoDao arriDao = new ArriendoDao(conect);
        Arriendo arriendoe = new Arriendo();
        
        HttpSession session = request.getSession();
        RequestDispatcher rd;
        if(session.getAttribute("usuario") == null) {
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }else{
            PeliculaDao peli = new PeliculaDao(conect);
            request.setAttribute("peliculas", peli.getComboPelicula());
          
            arriendoe.setRutUsuario(Rut);
            arriendoe.setNombreCliente(Nombre);
            arriendoe.setApellidoCliente(Apellido);
            arriendoe.setIdPelicula(Integer.parseInt(Pelicula));
            arriendoe.setDias(Integer.parseInt(Dias));
            
            arriDao.save(arriendoe);
            conect.disconnect();
            request.getRequestDispatcher("combocontroller").forward(request, response);
        } 
    }
    
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
