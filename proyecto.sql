-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-01-2019 a las 22:34:05
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `idCategoria` int(11) NOT NULL,
  `Estrellas` varchar(5) COLLATE utf8_bin DEFAULT NULL,
  `NombreEspañol` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  `NombreIngles` varchar(450) COLLATE utf8_bin NOT NULL,
  `NombrePortugues` varchar(450) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `idCiudad` int(11) NOT NULL,
  `Nombre_Ciudad` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`idCiudad`, `Nombre_Ciudad`) VALUES
(1, 'Puerto Varas'),
(2, 'Puerto Montt');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentar`
--

CREATE TABLE `comentar` (
  `idComentar` int(11) NOT NULL,
  `Calificacion` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Comentario` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `Servicio_Tutristico_idServicio_Tutristico` int(11) NOT NULL,
  `Usuario_idUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_fotos`
--

CREATE TABLE `detalle_fotos` (
  `Servicio_Turistico_idServicio_Turistico` int(11) NOT NULL,
  `Fotos_idFotos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_horario`
--

CREATE TABLE `detalle_horario` (
  `Horarios_idHorarios` int(11) NOT NULL,
  `Servicio_Turistico_idServicio_Turistico` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_redes_sociales`
--

CREATE TABLE `detalle_redes_sociales` (
  `Redes_Sociales_idRedes_Sociales` int(11) NOT NULL,
  `Servicio_Turistico_idServicio_Turistico` int(11) NOT NULL,
  `Url` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_servicio_adicional`
--

CREATE TABLE `detalle_servicio_adicional` (
  `Servicio_Turistico_idServicio_Turistico` int(11) NOT NULL,
  `Servicio_Adicional_idServicio_Adicional` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_video`
--

CREATE TABLE `detalle_video` (
  `Video_idVideo` int(11) NOT NULL,
  `Servicio_Turistico_idServicio_Turistico` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotos`
--

CREATE TABLE `fotos` (
  `idFotos` int(11) NOT NULL,
  `Foto` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `fotos`
--

INSERT INTO `fotos` (`idFotos`, `Foto`) VALUES
(1, '.jpg'),
(2, '.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

CREATE TABLE `horarios` (
  `idHorarios` int(11) NOT NULL,
  `Dia` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Hora` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `horarios`
--

INSERT INTO `horarios` (`idHorarios`, `Dia`, `Hora`) VALUES
(1, 'Lunes', '09:00:00'),
(2, 'Lunes', '18:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `redes_social`
--

CREATE TABLE `redes_social` (
  `idRedes_Sociales` int(11) NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `redes_social`
--

INSERT INTO `redes_social` (`idRedes_Sociales`, `Nombre`) VALUES
(1, 'Facebook'),
(2, 'Google');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_adicional`
--

CREATE TABLE `servicio_adicional` (
  `idServicio_Adicional` int(11) NOT NULL,
  `idioma` varchar(45) COLLATE utf8_bin NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `servicio_adicional`
--

INSERT INTO `servicio_adicional` (`idServicio_Adicional`, `idioma`, `Nombre`) VALUES
(1, 'Ingles', 'Parking'),
(1, 'Portugues', 'Estacionamiento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_turistico`
--

CREATE TABLE `servicio_turistico` (
  `idServicio_Turistico` int(11) NOT NULL,
  `Tipo` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Nombre` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Ubicacion` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `DescripcionEspañol` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  `DescripcionIngles` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  `DescripcionPortugues` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  `Categoria_idCategoria` int(11) NOT NULL,
  `Ciudad_idCiudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sugerencias`
--

CREATE TABLE `sugerencias` (
  `idSugerencias` int(11) NOT NULL,
  `Sugerencia` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `Usuario_idUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `sugerencias`
--

INSERT INTO `sugerencias` (`idSugerencias`, `Sugerencia`, `Usuario_idUsuario`) VALUES
(1, 'Probando DB', 2),
(2, 'Prueba 2', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Correo` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Contraseña` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Tipo_Usuario` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `Nombre`, `Correo`, `Contraseña`, `Tipo_Usuario`) VALUES
(1, 'Danilo Zuñiga', 'dazz5141@gmail.com', 'dazz1234', '1'),
(2, 'Juan Mansilla', 'juan@hotel.cl', 'juan1234', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `video`
--

CREATE TABLE `video` (
  `idVideo` int(11) NOT NULL,
  `Video` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `video`
--

INSERT INTO `video` (`idVideo`, `Video`) VALUES
(1, '.mp4'),
(2, '.avi');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idCategoria`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`idCiudad`);

--
-- Indices de la tabla `comentar`
--
ALTER TABLE `comentar`
  ADD PRIMARY KEY (`idComentar`),
  ADD KEY `fk_Comentar_Servicio_Tutristico1` (`Servicio_Tutristico_idServicio_Tutristico`),
  ADD KEY `fk_Comentar_Usuario1` (`Usuario_idUsuario`);

--
-- Indices de la tabla `detalle_fotos`
--
ALTER TABLE `detalle_fotos`
  ADD PRIMARY KEY (`Servicio_Turistico_idServicio_Turistico`,`Fotos_idFotos`),
  ADD KEY `fk_Servicio_Turistico_has_Fotos_Fotos1` (`Fotos_idFotos`);

--
-- Indices de la tabla `detalle_horario`
--
ALTER TABLE `detalle_horario`
  ADD PRIMARY KEY (`Horarios_idHorarios`,`Servicio_Turistico_idServicio_Turistico`),
  ADD KEY `fk_Horarios_has_Servicio_Turistico_Servicio_Turistico1` (`Servicio_Turistico_idServicio_Turistico`);

--
-- Indices de la tabla `detalle_redes_sociales`
--
ALTER TABLE `detalle_redes_sociales`
  ADD PRIMARY KEY (`Redes_Sociales_idRedes_Sociales`,`Servicio_Turistico_idServicio_Turistico`),
  ADD KEY `fk_Redes_Sociales_has_Servicio_Turistico_Servicio_Turistico1` (`Servicio_Turistico_idServicio_Turistico`);

--
-- Indices de la tabla `detalle_servicio_adicional`
--
ALTER TABLE `detalle_servicio_adicional`
  ADD PRIMARY KEY (`Servicio_Turistico_idServicio_Turistico`,`Servicio_Adicional_idServicio_Adicional`),
  ADD KEY `fk_Servicio_Turistico_has_Servicio_Adicional_Servicio_Adicion1` (`Servicio_Adicional_idServicio_Adicional`);

--
-- Indices de la tabla `detalle_video`
--
ALTER TABLE `detalle_video`
  ADD PRIMARY KEY (`Video_idVideo`,`Servicio_Turistico_idServicio_Turistico`),
  ADD KEY `fk_Video_has_Servicio_Turistico_Servicio_Turistico1` (`Servicio_Turistico_idServicio_Turistico`);

--
-- Indices de la tabla `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`idFotos`);

--
-- Indices de la tabla `horarios`
--
ALTER TABLE `horarios`
  ADD PRIMARY KEY (`idHorarios`);

--
-- Indices de la tabla `redes_social`
--
ALTER TABLE `redes_social`
  ADD PRIMARY KEY (`idRedes_Sociales`);

--
-- Indices de la tabla `servicio_adicional`
--
ALTER TABLE `servicio_adicional`
  ADD PRIMARY KEY (`idServicio_Adicional`,`idioma`);

--
-- Indices de la tabla `servicio_turistico`
--
ALTER TABLE `servicio_turistico`
  ADD PRIMARY KEY (`idServicio_Turistico`),
  ADD KEY `fk_Servicio_Turistico_Categoria1` (`Categoria_idCategoria`),
  ADD KEY `fk_Servicio_Turistico_Ciudad1` (`Ciudad_idCiudad`);

--
-- Indices de la tabla `sugerencias`
--
ALTER TABLE `sugerencias`
  ADD PRIMARY KEY (`idSugerencias`),
  ADD KEY `fk_Sugerencias_Usuario1` (`Usuario_idUsuario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- Indices de la tabla `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`idVideo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `idCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `idCiudad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `comentar`
--
ALTER TABLE `comentar`
  MODIFY `idComentar` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fotos`
--
ALTER TABLE `fotos`
  MODIFY `idFotos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `horarios`
--
ALTER TABLE `horarios`
  MODIFY `idHorarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `redes_social`
--
ALTER TABLE `redes_social`
  MODIFY `idRedes_Sociales` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `servicio_adicional`
--
ALTER TABLE `servicio_adicional`
  MODIFY `idServicio_Adicional` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `servicio_turistico`
--
ALTER TABLE `servicio_turistico`
  MODIFY `idServicio_Turistico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sugerencias`
--
ALTER TABLE `sugerencias`
  MODIFY `idSugerencias` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `video`
--
ALTER TABLE `video`
  MODIFY `idVideo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comentar`
--
ALTER TABLE `comentar`
  ADD CONSTRAINT `fk_Comentar_Servicio_Tutristico1` FOREIGN KEY (`Servicio_Tutristico_idServicio_Tutristico`) REFERENCES `servicio_turistico` (`idServicio_Turistico`),
  ADD CONSTRAINT `fk_Comentar_Usuario1` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`);

--
-- Filtros para la tabla `detalle_fotos`
--
ALTER TABLE `detalle_fotos`
  ADD CONSTRAINT `fk_Servicio_Turistico_has_Fotos_Fotos1` FOREIGN KEY (`Fotos_idFotos`) REFERENCES `fotos` (`idFotos`),
  ADD CONSTRAINT `fk_Servicio_Turistico_has_Fotos_Servicio_Turistico1` FOREIGN KEY (`Servicio_Turistico_idServicio_Turistico`) REFERENCES `servicio_turistico` (`idServicio_Turistico`);

--
-- Filtros para la tabla `detalle_horario`
--
ALTER TABLE `detalle_horario`
  ADD CONSTRAINT `fk_Horarios_has_Servicio_Turistico_Horarios1` FOREIGN KEY (`Horarios_idHorarios`) REFERENCES `horarios` (`idHorarios`),
  ADD CONSTRAINT `fk_Horarios_has_Servicio_Turistico_Servicio_Turistico1` FOREIGN KEY (`Servicio_Turistico_idServicio_Turistico`) REFERENCES `servicio_turistico` (`idServicio_Turistico`);

--
-- Filtros para la tabla `detalle_redes_sociales`
--
ALTER TABLE `detalle_redes_sociales`
  ADD CONSTRAINT `fk_Redes_Sociales_has_Servicio_Turistico_Redes_Sociales1` FOREIGN KEY (`Redes_Sociales_idRedes_Sociales`) REFERENCES `redes_social` (`idRedes_Sociales`),
  ADD CONSTRAINT `fk_Redes_Sociales_has_Servicio_Turistico_Servicio_Turistico1` FOREIGN KEY (`Servicio_Turistico_idServicio_Turistico`) REFERENCES `servicio_turistico` (`idServicio_Turistico`);

--
-- Filtros para la tabla `detalle_servicio_adicional`
--
ALTER TABLE `detalle_servicio_adicional`
  ADD CONSTRAINT `fk_Servicio_Turistico_has_Servicio_Adicional_Servicio_Adicion1` FOREIGN KEY (`Servicio_Adicional_idServicio_Adicional`) REFERENCES `servicio_adicional` (`idServicio_Adicional`),
  ADD CONSTRAINT `fk_Servicio_Turistico_has_Servicio_Adicional_Servicio_Turisti1` FOREIGN KEY (`Servicio_Turistico_idServicio_Turistico`) REFERENCES `servicio_turistico` (`idServicio_Turistico`);

--
-- Filtros para la tabla `detalle_video`
--
ALTER TABLE `detalle_video`
  ADD CONSTRAINT `fk_Video_has_Servicio_Turistico_Servicio_Turistico1` FOREIGN KEY (`Servicio_Turistico_idServicio_Turistico`) REFERENCES `servicio_turistico` (`idServicio_Turistico`),
  ADD CONSTRAINT `fk_Video_has_Servicio_Turistico_Video1` FOREIGN KEY (`Video_idVideo`) REFERENCES `video` (`idVideo`);

--
-- Filtros para la tabla `servicio_turistico`
--
ALTER TABLE `servicio_turistico`
  ADD CONSTRAINT `fk_Servicio_Turistico_Categoria1` FOREIGN KEY (`Categoria_idCategoria`) REFERENCES `categoria` (`idCategoria`),
  ADD CONSTRAINT `fk_Servicio_Turistico_Ciudad1` FOREIGN KEY (`Ciudad_idCiudad`) REFERENCES `ciudad` (`idCiudad`);

--
-- Filtros para la tabla `sugerencias`
--
ALTER TABLE `sugerencias`
  ADD CONSTRAINT `fk_Sugerencias_Usuario1` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
